// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#ifndef _STDAFX_H_
#define _STDAFX_H_

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <conio.h>
#include <ctime>

#include "SecureInput.h"
#include "Product.h"
#include "ProductStruct.h"
#include "BaseOut.h"
#include "Sort.h"

// TODO: reference additional headers your program requires here

#endif
