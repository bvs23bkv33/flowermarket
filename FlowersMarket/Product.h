#ifndef _PRODUCT_H_
#define _PRODUCT_H_

#include "ProductStruct.h"
#include "BaseOut.h"

class Product
{
	friend class Sort;
protected:
	ProdStruct prod;
	FILE *filebin;
	char curDate[11]; //format: year/month/day; examples: 1990/01/23 | 2013/11/07 - OK
	char curTime[9];  //format: hour/minute/second; example: 05/33/59 | 17/45/00 - OK

	SecureInput secin;
public:
	virtual int CreateFile() = 0; // - OK
	virtual void AddToFile() = 0;
	void CreateProduct(); // - OK
	void CheckDateTime(); // - OK
	virtual ~Product() {};
	void setName(const char *);
	void setPrice(double);
	void setQuantity(unsigned int);
	char * getName();
	double getPrice();
	unsigned int getQuantity();
};

class ProductAlive : public Product
{
public:
	int CreateFile(); // - OK
	void AddToFile(); // - OK

	friend void AliveOut::Show(Product *obj, int amount);
};

class ProductTools : public Product
{
public:
	int CreateFile(); // - OK
	void AddToFile(); // - OK
};

#endif



























//class Base {
//public:
//	friend ostream& operator << (ostream& o, const Base& b);
//	// ...
//protected:
//	virtual void print(ostream& o) const
//	{
//		...
//	}
//};
///* make sure to put this function into the header file */
//inline std::ostream& operator<< (std::ostream& o, const Base& b)
//{
//	b.print(o); // delegate the work to a polymorphic member function.
//	return o;
//}
//
//class Derived : public Base {
//protected:
//	virtual void print(ostream& o) const
//	{
//		...
//	}
//};


//// friend class
//#include <iostream>
//using namespace std;
//
//class Square;
//
//class Rectangle {
//	int width, height;
//public:
//	int area()
//	{
//		return (width * height);
//	}
//	void convert(Square a);
//};
//
//class Square {
//	friend class Rectangle;
//private:
//	int side;
//public:
//	Square(int a) : side(a) {}
//};
//
//void Rectangle::convert(Square a) {
//	width = a.side;
//	height = a.side;
//}
//
//int main() {
//	Rectangle rect;
//	Square sqr(4);
//	rect.convert(sqr);
//	cout << rect.area();
//	return 0;
//}