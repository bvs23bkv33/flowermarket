#ifndef _PRODUCTSTRUCT_H_
#define _PRODUCTSTRUCT_H_

struct ProdStruct
{
	static const int nameLen = 50;
	char name[nameLen + 1];
	unsigned int quantity;
	double price;
};

#endif

