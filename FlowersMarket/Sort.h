#ifndef _SORT_H_
#define _SORT_H_

class Sort {
	static const int compare_array_size = 6;

	static int compare_products_by_name_ascending(const void *, const void *);
	static int compare_products_by_name_descending(const void *, const void *);
	static int compare_products_by_quantity_ascending(const void *, const void *);
	static int compare_products_by_quantity_descending(const void *, const void *);
	static int compare_products_by_price_ascending(const void *, const void *);
	static int compare_products_by_price_descending(const void *, const void *);

	static int (*comparator_array[compare_array_size])(const void *, const void *);

	static int composit_compare(const void *, const void *);
public:
	static void set_compare();
	static void quick(Product **, int);
};

#endif