#include "stdafx.h"

//PRODUCT

void Product::CreateProduct()
{
	cout << "Input product\n";
	secin.Input("Input product name: ", prod.name, prod.nameLen);
	secin.Input("Input product price: ", prod.price);
	secin.Input("Input product quantity: ", prod.quantity);
};

void Product::CheckDateTime()
{
	time_t seconds = time(NULL);
	tm *timedata = localtime(&seconds);
	char buff[5];

	//DATE
	sprintf(curDate, "%d", (1900 + timedata->tm_year));
	curDate[4] = '/';
	curDate[5] = '\0';

	if (timedata->tm_mon < 9)
	{
		curDate[5] = '0';
		curDate[6] = '\0';
		sprintf(buff, "%d", (1 + timedata->tm_mon));
		strcat(curDate, buff);
	}
	else
	{
		sprintf(buff, "%d", (1 + timedata->tm_mon));
		strcat(curDate, buff);
	}
	curDate[7] = '/';
	curDate[8] = '\0';

	if (timedata->tm_mday < 10)
	{
		curDate[8] = '0';
		curDate[9] = '\0';
		sprintf(buff, "%d", (timedata->tm_mday));
		strcat(curDate, buff);
	}
	else
	{
		sprintf(buff, "%d", (timedata->tm_mday));
		strcat(curDate, buff);
	}

	//TIME
	if (timedata->tm_hour < 10)
	{
		curTime[0] = '0';
		curTime[1] = '\0';
		sprintf(buff, "%d", timedata->tm_hour);
		strcat(curTime, buff);
	}
	else
	{
		sprintf(curTime, "%d", timedata->tm_hour);
	}
	curTime[2] = '/';
	curTime[3] = '\0';

	if (timedata->tm_min < 10)
	{
		curTime[3] = '0';
		curTime[4] = '\0';
		sprintf(buff, "%d", timedata->tm_min);
		strcat(curTime, buff);
	}
	else
	{
		sprintf(buff, "%d", timedata->tm_min);
		strcat(curTime, buff);
	}
	curTime[5] = '/';
	curTime[6] = '\0';

	if (timedata->tm_sec < 10)
	{
		curTime[6] = '0';
		curTime[7] = '\0';
		sprintf(buff, "%d", timedata->tm_sec);
		strcat(curTime, buff);
	}
	else
	{
		sprintf(buff, "%d", timedata->tm_sec);
		strcat(curTime, buff);
	}
};

//ALIVE

int ProductAlive::CreateFile()
{
	if ((filebin = fopen("data\\alive.db", "wb")) == 0)
	{
		cout << "Error! File don't create!\nPress any key for exit\n";
		system("pause");
		return 1;
	}
	return 0;
};

void ProductAlive::AddToFile()
{
	/*1) char ������ �� 9 �������� (������� '\0') � ��������� ���������� (� �������: �����, ����������� � �.�.)
	  2) int �������� � ����������� �������.
	  3) ���� � ����� ��������� �����������. (��� ��������� ����, �������� ���)
	  4) ����� ���������� 400 ���� ��� �����.
	  5) ����� ���� ������.*/
	
	bool newFile = false;

	if (filebin == 0 && (filebin = fopen("data\\alive.db", "r+b")) == 0)
	{
		CreateFile();
		newFile = true;
	}

	CreateProduct();

	if (newFile == true)
	{
		char fileName[9] = "alive.db";
		int amount = 1;

		fwrite(&fileName, sizeof(char) * 9, 1, filebin);
		fwrite(&amount, sizeof(int), 1, filebin);
		fwrite(&curDate, sizeof(char) * 11, 1, filebin);
		fwrite(&curTime, sizeof(char) * 9, 1, filebin);

		fseek(filebin, 400, SEEK_CUR); //empty 400 bytes

		fwrite(&prod, sizeof(ProdStruct), 1, filebin);

		fclose(filebin);
	}
	else
	{
		ProdStruct prodBuff;
		int counter(0), prodCounter(0);
		int amount;

		fseek(filebin, sizeof(char) * 9, SEEK_SET);

		fread(&amount, sizeof(int), 1, filebin);

		fseek(filebin, (sizeof(char) * 20) + 400, SEEK_CUR);
		while (counter < amount)
		{
			fread(&prodBuff, sizeof(ProdStruct), 1, filebin);
			if (!strcmp(prodBuff.name, prod.name) && prodBuff.price == prod.price)
			{
				prodBuff.quantity += prod.quantity;

				fseek(filebin, sizeof(char) * 9, SEEK_SET);
				fseek(filebin, sizeof(int), SEEK_CUR);
				fseek(filebin, (sizeof(char) * 20) + 400, SEEK_CUR);
				fseek(filebin, sizeof(ProdStruct) * prodCounter, SEEK_CUR);

				fwrite(&prodBuff, sizeof(ProdStruct), 1, filebin);
				fseek(filebin, sizeof(char) * 9, SEEK_SET);
				fwrite(&amount, sizeof(int), 1, filebin);
				
				CheckDateTime();

				fwrite(&curDate, sizeof(char) * 11, 1, filebin);
				fwrite(&curTime, sizeof(char) * 9, 1, filebin);

				break;
			}

			counter++;
			prodCounter++;

			if (counter == amount)
			{
				amount++;

				fseek(filebin, 0, SEEK_END);
				fwrite(&prod, sizeof(ProdStruct), 1, filebin);
				fseek(filebin, sizeof(char) * 9, SEEK_SET);
				fwrite(&amount, sizeof(int), 1, filebin);

				fseek(filebin, sizeof(char) * 9, SEEK_SET);
				fwrite(&amount, sizeof(int), 1, filebin);

				CheckDateTime();

				fwrite(&curDate, sizeof(char) * 11, 1, filebin);
				fwrite(&curTime, sizeof(char) * 9, 1, filebin);

				amount--;
			}
		}

		fclose(filebin);
	}
};

//TOOLS

int ProductTools::CreateFile()
{
	if ((filebin = fopen("data\\tools.db", "wb")) == 0)
	{
		cout << "Error! File don't create!\nPress any key for exit\n";
		system("pause");
		return 1;
	}
	return 0;
};

void ProductTools::AddToFile()
{
	/*1) char ������ �� 9 �������� (������� '\0') � ��������� ���������� (� �������: �����, ����������� � �.�.)
	2) int �������� � ����������� �������.
	3) ���� � ����� ��������� �����������. (��� ��������� ����, �������� ���)
	4) ����� ���������� 400 ���� ��� �����.
	5) ����� ���� ������.*/

	bool newFile = false;

	if (filebin == 0 && (filebin = fopen("data\\tools.db", "rb")) == 0)
	{
		CreateFile();
		newFile = true;
	}
	cout << sizeof(ProdStruct);
	CreateProduct();

	if (newFile == true)
	{
		char fileName[9] = "tools.db";
		int amount = 1;

		fwrite(&fileName, sizeof(char) * 9, 1, filebin);
		fwrite(&amount, sizeof(int), 1, filebin);
		fwrite(&curDate, sizeof(char) * 11, 1, filebin);
		fwrite(&curTime, sizeof(char) * 9, 1, filebin);

		fseek(filebin, 400, SEEK_CUR); //empty 400 bytes

		fwrite(&prod, sizeof(ProdStruct), 1, filebin);

		fclose(filebin);
	}
	else
	{
		ProdStruct prodBuff;
		int counter(0), prodCounter(0);
		int amount;

		fseek(filebin, sizeof(char) * 9, SEEK_SET);

		fread(&amount, sizeof(int), 1, filebin);

		fseek(filebin, (sizeof(char) * 20) + 400, SEEK_CUR);

		while (counter < amount)
		{
			fread(&prodBuff, sizeof(ProdStruct), 1, filebin);
			if (prodBuff.name == prod.name && prodBuff.price == prod.price)
			{
				prodBuff.quantity += prod.quantity;

				fseek(filebin, sizeof(char) * 9, SEEK_SET);
				fseek(filebin, sizeof(int), SEEK_CUR);
				fseek(filebin, (sizeof(char) * 20) + 400, SEEK_CUR);
				fseek(filebin, sizeof(ProdStruct) * prodCounter, SEEK_CUR);

				fwrite(&prodBuff, sizeof(ProdStruct), 1, filebin);
				fseek(filebin, sizeof(char) * 9, SEEK_SET);
				fwrite(&amount + 1, sizeof(int), 1, filebin);

				CheckDateTime();

				fwrite(&curDate, sizeof(char) * 11, 1, filebin);
				fwrite(&curTime, sizeof(char) * 9, 1, filebin);

				break;
			}

			counter++;
			prodCounter++;

			if (counter == amount)
			{
				fseek(filebin, 0, SEEK_END);
				fwrite(&prod, sizeof(ProdStruct), 1, filebin);
				fseek(filebin, sizeof(char) * 9, SEEK_SET);
				fwrite(&amount + 1, sizeof(int), 1, filebin);
			}
		}

		fclose(filebin);
	}
};

void Product::setName(const char * name) {
	strcpy_s(this->prod.name, prod.nameLen + 1, name);
};

void Product::setPrice(double price) {
	this->prod.price = price;
};

void Product::setQuantity(unsigned int quantity) {
	this->prod.quantity = quantity;
};

char * Product::getName() {
	return this->prod.name;
};

double Product::getPrice() {
	return this->prod.price;
};

unsigned int Product::getQuantity() {
	return this->prod.quantity;
};