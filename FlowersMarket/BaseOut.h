#ifndef _BASEOUT_H_
#define _BASEOUT_H_

class Product;

class BaseOut
{
public:
	virtual void Show(Product *obj, int amount) = 0;
	virtual ~BaseOut() {};
};

class AliveOut : public BaseOut
{
public:
	void Show(Product *obj, int amount);
};

class ToolsOut : public BaseOut
{
public:
	void Show(Product *obj, int amount);
};

#endif