#include "stdafx.h"

int Sort::compare_products_by_name_ascending(const void * product1, const void * product2) {
	return strcmp((*(Product**)product1)->prod.name, (*(Product**)product2)->prod.name);
};

int Sort::compare_products_by_name_descending(const void * product1, const void * product2) {
	return strcmp((*(Product**)product2)->prod.name, (*(Product**)product1)->prod.name);
};

int Sort::compare_products_by_quantity_ascending(const void * product1, const void * product2) {
	return ((*(Product**)product1)->prod.quantity - (*(Product**)product2)->prod.quantity);
};

int Sort::compare_products_by_quantity_descending(const void * product1, const void * product2) {
	return ((*(Product**)product2)->prod.quantity - (*(Product**)product1)->prod.quantity);
};

int Sort::compare_products_by_price_ascending(const void * product1, const void * product2) {
	double result = ((*(Product**)product1)->prod.price - (*(Product**)product2)->prod.price);
	if (result < 0) return -1;
	if (result > 0) return 1;
	return 0;
};

int Sort::compare_products_by_price_descending(const void * product1, const void * product2) {
	double result = ((*(Product**)product2)->prod.price - (*(Product**)product1)->prod.price);
	if (result < 0) return -1;
	if (result > 0) return 1;
	return 0;
};

int (*Sort::comparator_array[compare_array_size])(const void * product1, const void * product2) = {
	compare_products_by_price_ascending,
	compare_products_by_price_descending,
	compare_products_by_quantity_descending,
	compare_products_by_quantity_ascending,
	compare_products_by_name_ascending,
	compare_products_by_name_descending,
};

int Sort::composit_compare(const void * product1, const void * product2) {
	int result = 0;
	for (int i = 0; i < compare_array_size; ++i) {
		if (comparator_array[i]) {
			int result = comparator_array[i](product1, product2);
			if (result) return result;
		}
	}
	return result;
};

void Sort::set_compare() {
	//here goes code to set compare functions array as user wants
	//what this func will accept as parameters?
};

void Sort::quick(Product ** array, int size) {
	qsort(array, size, sizeof(Product*), composit_compare);
};